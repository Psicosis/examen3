package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.Zona;
import com.example.BlogRestaurante.repositories.ZonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by July on 11/28/2017.
 */
@Service
public class ZonaServiceImpl implements ZonaService {

    @Autowired
    ZonaRepository zonaRepository;

    @Override
    public Iterable<Zona> listAllOptions() {
        return zonaRepository.findAll();
    }

    /*
    @Autowired
    public Iterable<Choice> listOptionsById(Integer id) {return choiceRepository.findAll(); }
*/
    @Override
    public Zona getZonaById(Integer id) {
        return zonaRepository.findById(id);
    }

    @Override
    public Zona saveZona(Zona option) {
        return zonaRepository.save(option);
    }

    @Override
    public void deleteZona(Integer id) {

        zonaRepository.delete(id);
    }




}
