package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.Pedido;


public interface PedidoService {

    Iterable<Pedido> listAllOptions();
    //Iterable<Pedido> listOptionsFilt(Integer id);
    Pedido getRestaurantById(Integer id);
    Pedido saveRestaurant(Pedido option);
    void deleteRestaurant(Integer id);
}
