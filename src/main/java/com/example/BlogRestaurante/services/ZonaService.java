package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.Zona;

/**
 * Created by July on 11/28/2017.
 */
public interface ZonaService {

    Iterable<Zona> listAllOptions();
    Zona getZonaById(Integer id);
    Zona saveZona(Zona option);
    void deleteZona(Integer id);
}
