package com.example.BlogRestaurante.repositories;

import com.example.BlogRestaurante.entities.Zona;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

/**
 * Created by July on 11/28/2017.
 */
@Transactional
public interface ZonaRepository extends JpaRepository<Zona,Integer>{
    Zona findById(Integer id);
}
